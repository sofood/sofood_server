var moment = require('moment'),
    mongoose = require('mongoose'),
    faker = require('faker'),
    request = require('request'),
    Flickr = require('flickrapi');

var UserSchema = mongoose.model('User');
var PostSchema = mongoose.model('Post');

var tools = {
    resetPopularityAllPosts: function() {
        PostSchema.find({}).exec(function(err, posts) {
            posts.forEach(function(post) {
                post.save(function(err, p) {
                    console.log('[feed.js:tools.resetPopularityAllPosts()]: ', p.popularity);
                });
            });
        });
    },
    resetMacrosAllPosts: function() {
        PostSchema.find({}).exec(function(err, posts) {
            posts.forEach(function(post) {
                post.macros = {
                    calories: 1,
                    protein: 1,
                    carbohydrates: 1,
                    fats: 1
                };
                post.save();
            });
        });
    },
    setPostsRandomFoodPictures: function() {
        PostSchema.find({}).exec(function(err, posts) {
            posts.forEach(function(post, index) {
                console.log('setPostsRandomFoodPictures ' + (((index) % 10) + 1));
                post.image = 'http://lorempixel.com/640/480/food/' + (((index) % 10) + 1);
                post.save();
            });
        });
    },
    generateRandomUsers: function(numRandomUsers) {
        for (var i = 0; i < numRandomUsers; i = i + 1) {
            var newUser = new UserSchema();
            newUser.picture = faker.image.avatar();
            newUser.facebook = faker.random.number();
            newUser.email = faker.internet.email();
            newUser.name = faker.name.findName();
            newUser.save();

            console.log('Generating random user: ', i);
        }
    },
    // Requires existing users in database.
    generateRandomPosts: function(numRandomUsers, numRandomPosts) {

        var randomUsers;
        var flickrOptions = {
            api_key: '4e55ad959e172830e6670c67f561336a',
            secret: 'c1568e63b0d89d2d'
        };
        Flickr.tokenOnly(flickrOptions, function(error, flickr) {
            flickr.photos.search({
                tags: 'food',
                per_page: 500,
                extras: 'url_z'
            }, function(err, result) {
                console.log(err);
                var flickrPhotos = result.photos.photo;

                UserSchema
                    .find({})
                    .limit(numRandomUsers)
                    .exec(function(err, users) {
                        if (err) return console.log('generateRandomPosts err: ', err);
                        if (!users.length) return console.log('generateRandomPosts: NO USERS IN DB!');
                        randomUsers = users;

                        for (var i = 0; i < numRandomPosts; i = i + 1) {
                            var rand = Math.floor(Math.random() * randomUsers.length);
                            var newPost = new PostSchema();
                            newPost.image = flickrPhotos[i].url_z;

                            // Backup
                            if (!newPost.image)
                                newPost.image = 'http://lorempixel.com/640/480/food/' + (((i) % 10) + 1);

                            console.log(newPost.image);
                            //newPost.image = 'http://lorempixel.com/640/480/food/' + (((i) % 10) + 1);
                            //newPost.image = 'https://source.unsplash.com/category/food/all/640x480';
                            newPost.caption = faker.commerce.productAdjective();
                            newPost.user = randomUsers[rand]._id;
                            newPost.macros =  {
                                calories: faker.random.number(800),
                                protein: faker.random.number(100),
                                carbohydrates: faker.random.number(100),
                                fats: faker.random.number(80)
                            };

                            console.log('Generating random post: ', i);
                            newPost.save();
                        }
                    });
            });


        });
    },
    deleteAllPosts: function() {
        PostSchema.find({}).exec(function(err, posts) {
            posts.forEach(function(post) {
                post.remove();
            });
        });
    },
    deleteAllUsers: function() {
        UserSchema.find({}).exec(function(err, users) {
            users.forEach(function(user) {
                user.remove();
            });
        });
    },
    removePostsWithNoUsers: function() {
        console.log('removePostWithNoUsers...');
        PostSchema.find({ user: { $exists: false } }).exec(function(err, posts) {
            posts.forEach(function(post) {
                console.log(post);
            });
        });
    }
};
//tools.removePostsWithNoUsers();
//tools.generateRandomPosts(50, 100);
//tools.generateRandomUsers(50);
//tools.setPostsRandomFoodPictures();

// Helper functions
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}
function getRandomArbitrary(min, max) {
    //return Math.random() * (max - min) + min;
    return Math.floor(Math.exp(Math.random() * Math.log(max - min + 1))) + min;
}
