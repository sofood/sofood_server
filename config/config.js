var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'sofood-server'
    },
    port: 2999,
    db: 'mongodb://sofoodAdmin:!sofoodAdmin#@192.241.202.71:27017/sofood'
  },

  test: {
    root: rootPath,
    app: {
      name: 'sofood-server'
    },
    port: 2999,
    db: 'mongodb://sofoodAdmin:!sofoodAdmin#@192.241.202.71:27017/sofood'
  },

  production: {
    root: rootPath,
    app: {
      name: 'sofood-server'
    },
    port: 2999,
    db: 'mongodb://sofoodAdmin:!sofoodAdmin#@192.241.202.71:27017/sofood'
  }
};

module.exports = config[env];
