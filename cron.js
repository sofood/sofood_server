var moment = require('moment'),
    FeedCtrl = require('./app/controllers/feed'),
    CronJob = require('cron').CronJob;

var job = new CronJob({
    //Testing: Every 10 seconds - cronTime: '*/10 * * * * *',
    cronTime: '0 */5 * * * *', // Runs onTick() every 5 minutes.
    onTick: function() {
        console.log('[cron.js]: Rebuilding trending posts - ' + moment().toDate());
        FeedCtrl.trending.newTree();
        FeedCtrl.trending.buildTree();
    },
    start: false,
    timeZone: 'America/Los_Angeles'
});
job.start();
