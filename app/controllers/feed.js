var express = require('express'),
    router = express.Router(),
    cors = require('cors'),
    mongoose = require('mongoose'),
    jwt = require('jsonwebtoken'),
    expressJwt = require('express-jwt'),
    moment = require('moment'),
    createTree = require('functional-red-black-tree'),
    CronJob = require('cron').CronJob;

var PostSchema = mongoose.model('Post');
var UserSchema = mongoose.model('User');

var SOFOOD_SECRET = '#rub_a_dubDub_thanks_forthe_grub!';
var TREE_MAX = 100;
var TREE_PREVIOUS_DAYS = 14;

// The number of extra posts to check to build random throwbacks.
// Useful when there aren't many posts in the database so that the same
// post is not returned as a throwback every time.
// Increase to get higher throwback variation. Default = 2.
var MIN_THROWBACK_RANDOMIZER = 2;

module.exports = function (app) {
    app.use(cors());
    app.use('/api/feed', router);
};

// Query: ?page=1&per_page=10
router.get('/me', expressJwt({ secret: SOFOOD_SECRET }), function(req, res, next) {

    if (!req.query.page)
        return res.status(400).send({"err": "'page' query must be greater than 0"});
    else if (!req.query.per_page)
        return res.status(400).send({"err": "'per_page' query must be a number greater than 0"});

    UserSchema.findById(req.user._id, function(err, user) {
        if (err) return next(err);
        if (!user) return res.status(404).send({"err": "user not found"});

        //console.log(user);

        PostSchema.find({user: user._id})
            .sort('-timeStamp.created')
            .skip((req.query.page - 1) * req.query.per_page)
            .limit(req.query.per_page)
            .exec(function(err, posts) {
                if(err) next(err);

                for (var i = 0; i < posts.length; i = i + 1) {
                    posts[i] = posts[i].toJSON();
                    posts[i].userName = user.name[0];
                    posts[i].userPicture = user.picture;
                }
                return res.status(200).send(posts);
        });
    });
});

// Query: ?page=1&per_page=10
router.get('/friends', expressJwt({ secret: SOFOOD_SECRET }), function(req, res, next) {

    if (!req.query.page)
        return res.status(400).send({"err": "'page' query must be greater than 0"});
    else if (!req.query.per_page)
        return res.status(400).send({"err": "'per_page' query must be a number greater than 0"});

    UserSchema.findById(req.user._id, function(err, user) {
        if (err) return next(err);
        if (!user) return res.status(404).send({"err": "user not found"});

        PostSchema.find({user: {$in:user.following}})
            .sort('-timeStamp.created')
            .skip((req.query.page - 1) * req.query.per_page)
            .limit(req.query.per_page)
            .populate('user')
            .exec(function(err, posts) {
                if(err) next(err);
                for (var i = 0; i < posts.length; i = i + 1) {
                    posts[i] = posts[i].toJSON();
                    posts[i].userName = posts[i].user.name[0];
                    posts[i].userPicture = posts[i].user.picture;
                }
                return res.status(200).send(posts);
        });
    });
});

// Query: ?page=1&per_page=10
router.get('/trending', function(req, res, next) {

    if (!req.query.page)
        return res.status(400).send({"err": "'page' query must be greater than 0"});
    else if (!req.query.per_page)
        return res.status(400).send({"err": "'per_page' query must be a number greater than 0"});

    var trendingPosts = [];
    var skip = (req.query.page - 1) * req.query.per_page;
    var limit = req.query.per_page;

    if (skip || limit) {
        //console.log('GET Position: ' + skip);
        var it = trending.tree.at(skip);
        for (var i = 0; i < limit; i = i + 1) {
            trendingPosts.push(it.value);
            it.next();
        }
    }
    else {
        //console.log('ELSE Return length: ' + trending.tree.values.length);
        return res.status(200).send(trending.tree.values);
    }

    //console.log('OUTER Return length: ' + trendingPosts.length);
    return res.status(200).send(trendingPosts);
});

/**
*   trending
*   --------
*   tree:           tree object (instantiated at creation of trending)
*   newTree():      instantiate new empty tree
*   buildTree():    build a tree (fill with posts)
*   updateTree():   update a tree by passing in an updated post
*/
var trending = {
    tree: createTree(function(a, b) {
        return b.popularity - a.popularity;
    }),
    newTree: function() {
        this.tree = createTree(function(a, b) {
            return b.popularity - a.popularity;
        });
    },
    buildTree: function() {

        /** TODO: Throwbacks
            +>   Generate a random number of throwbacks numThrowbacks where the number
                of throwbacks does not exceed TREE_MAX / 10.

            >   Push (TREE_MAX - numThrowbacks) posts into the tree with new field
                'isThrowback' = false.

            >   Push numThrowbacks posts into the tree with 'isThrowback' = true and
                a random popularity value between the lowest popularity in tree and
                the highest popularity in the tree.
        */

        console.log('[feed.js:buildTree()] Building tree...');

        var upperBound = moment().toDate();
        var lowerBound = moment().subtract(TREE_PREVIOUS_DAYS, 'days').toDate();
        var postIdsNotThrowbacks = [];

        // numThrowbacks set to correct value after we get the number of posts.
        var numThrowbacks = 0;

        PostSchema
            .find({
                'timeStamp.created': {
                    $gte: lowerBound,
                    $lt: upperBound
                }
            })
            .sort('-timeStamp.created')
            .limit(TREE_MAX - numThrowbacks)
            .populate('user')
            .exec(function(err, posts) {
                if (err) return console.log(err);

                numThrowbacks = getRandomInt(1, Math.ceil(posts.length / 10));

                posts.forEach(function(value, index, array) {
                    var duration = moment.duration(moment().diff(value.timeStamp.created));
                    var hoursAgo = duration.asHours();

                    value = value.toObject();
                    value.userName = value.user.name[0];
                    value.userPicture = value.user.picture;
                    value.user = value.user._id;
                    //value.popularity = (value.favoritedBy.count * value.favoritedBy.count) - hoursAgo;
                    value.isThrowback = false;
                    trending.tree = trending.tree.insert({ '_id': value._id, 'popularity': value.popularity, 'isThrowback': false }, value);
                    postIdsNotThrowbacks.push(value._id);
                });

                //console.log('numNotThrowbacks: ', postIdsNotThrowbacks.length);
                console.log('[feed.js:buildTree()] Number of throwbacks: ', numThrowbacks);

                if (numThrowbacks) {
                    PostSchema
                        .find({
                            '_id': {
                                $nin: postIdsNotThrowbacks
                            }
                        })
                        .sort('-favoritedBy.count')
                        .limit(numThrowbacks + MIN_THROWBACK_RANDOMIZER)
                        .populate('user')
                        .exec(function(err, throwbacks) {
                            if (err) return console.log(err);

                            var minPopularity = trending.tree.end.key.popularity;
                            var maxPopularity = trending.tree.begin.key.popularity;
                            console.log('[feed.js:buildTree()] Min Popularity: ', minPopularity);
                            console.log('[feed.js:buildTree()] Max Popularity: ', maxPopularity);

                            while (throwbacks.length > numThrowbacks) {
                                var randIndex = Math.floor(Math.random() * throwbacks.length);
                                throwbacks.splice(randIndex, 1);
                            }

                            //console.log('throwbacks: ', throwbacks);

                            throwbacks.forEach(function(value, index, array) {
                                value = value.toObject();
                                value.userName = value.user.name[0];
                                value.userPicture = value.user.picture;
                                value.user = value.user._id;

                                value.popularity = getRandomArbitrary(minPopularity, maxPopularity);
                                value.isThrowback = true;
                                console.log('[feed.js:buildTree()] Inserting throwback; popularity: ', value.popularity);
                                trending.tree = trending.tree.insert({ '_id': value._id, 'popularity': value.popularity, 'isThrowback': true }, value);
                            });

                            //console.log('------');
                            //console.log('[feed.js:buildTree()] Tree Keys: ');
                            //console.log(trending.tree.keys);
                            //console.log('[feed.js:buildTree()] Tree Values: ', trending.tree.values);
                        });
                }
            });
    },
    updateTree: function(post) {
        var it = trending.tree.begin;
        var minIt = trending.tree.end;
        var foundPost = false;

        var duration = moment.duration(moment().diff(post.timeStamp.created));
        var hoursAgo = duration.asHours();
        //post.popularity = (post.favoritedBy.count * post.favoritedBy.count) - hoursAgo;

        while (it.hasNext) {
            if (it.key._id.toString() === post._id.toString()) {
                it.key.popularity = post.popularity;
                it.value = post;

                foundPost = true;
            }
            it.next();
        }

        // Push post into tree if it's greater than the least popularity node;
        // pop off least popularity post if we've reach tree's limit
        if (foundPost === false && minIt.key.popularity < post.popularity) {
            if (trending.tree.length >= TREE_MAX) {
                console.log('[feed.js:updateTree()] REMOVING minimum popularity post: ', minIt.key._id, ' :: ', minIt.key.popularity);
                trending.tree = trending.tree.remove(minIt.key);
            }
            console.log('[feed.js:updateTree()] INSERTING higher popularity post: ', post._id, ' :: ', post.popularity);
            trending.tree = trending.tree.insert({ '_id': post._id, 'popularity': post.popularity }, post);
        }

        //console.log('Trending tree keys: ', trending.tree.keys);
    },
    testUpdate: function() {
        trending.updateTree({
            "_id": "564b84d71252603a59d4372a__",
            "image": "http://i.imgur.com/Y5ycwfM.jpg",
            "caption": "Friend2 Second",
            "user": "564b83b01252603a59d43724",
            "__v": 0,
            "timeStamp": {
              "updated": "2015-11-17T19:49:43.449Z",
              "created": "2015-11-17T19:49:43.448Z"
            },
            "comments": [],
            "favoritedBy": {
              "count": 5,
              "users": []
            }
        });
    }
};

// Initialize tree at start.
trending.buildTree();

//tools.resetPopularityAllPosts();
//tools.resetMacrosAllPosts();

module.exports.trending = trending;

// Helper functions
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}
function getRandomArbitrary(min, max) {
    //return Math.random() * (max - min) + min;
    return Math.floor(Math.exp(Math.random() * Math.log(max - min + 1))) + min;
}
