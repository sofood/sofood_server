var express = require('express'),
  cors = require('cors'),
  router = express.Router(),
  mongoose = require('mongoose');

module.exports = function (app) {
  app.use('/', router);
};

/*
router.get('/', function (req, res, next) {
  res.render('index', {
      title: 'Welcome to the SoFood public API! There is currently no documentation, please check back later.',
  });
});
*/

router.get('/', cors(),function (req, res, next) {
    res.redirect('swagger/index.html');
});
