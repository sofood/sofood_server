var express = require('express'),
    router = express.Router(),
    cors = require('cors'),
    mongoose = require('mongoose'),
    jwt = require('jsonwebtoken'),
    expressJwt = require('express-jwt');

var CommentSchema = mongoose.model('Comment');
var UserSchema = mongoose.model('User');
var PostSchema = mongoose.model('Post');

var SOFOOD_SECRET = '#rub_a_dubDub_thanks_forthe_grub!';

module.exports = function (app) {
    app.use(cors());
    app.use('/api/comment', router);
};


router.post('/', expressJwt({ secret: SOFOOD_SECRET }), function(req, res, next) {

    console.log(req.user._id);

    var missing = [];
    if (!req.body.text)
        missing.push("missing comment text");
    if (!req.body.postId)
        missing.push("missing post id");
    if (missing.length) {
        return res.status(400).send({
            "err": missing.join(', ')
        });
    }

    UserSchema.findById(req.user._id, function(err, user) {
        if (err) return next(err);
        if (!user) return res.status(404).send({"err": "User not found"});

        var newComment = new CommentSchema();
        newComment.text = req.body.text;
        newComment.user = req.user._id;
        newComment.post = req.body.postId;

        newComment.save(function(err, comment) {
            if (err) return next(err);
            return res.status(200).send(comment);
        });

    });
});

router.get('/:commentId', function(req, res, next) {

    if (!req.params.commentId)
        return res.status(400).send({"err": "missing commentId"});

    CommentSchema.findById(req.params.commentId, function(err, comment) {
        if (err) return next(err);
        if (!comment) return res.status(404).send({"err": "comment not found"});

        return res.status(200).send(comment);
    });
});

router.delete('/:commentId', expressJwt({ secret: SOFOOD_SECRET }), function(req, res, next) {

    if (!req.params.commentId)
        return res.status(400).send({"err": "missing commentId"});

    CommentSchema.findById(req.params.commentId, function(err, comment) {
        if (err) return next(err);
        if (!comment) return res.status(404).send({"err": "comment not found"});
        if (comment.user != req.user._id) return res.status(400).send({"err": "Cannot delete another user's comment."});

        comment.remove(function(err) {
            if (err) return next(err);
            return res.status(200).end();
        });
    });
});
