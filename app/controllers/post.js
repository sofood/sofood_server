var express = require('express'),
    router = express.Router(),
    cors = require('cors'),
    mongoose = require('mongoose'),
    jwt = require('jsonwebtoken'),
    expressJwt = require('express-jwt');
    FeedCtrl = require('./feed.js');

var PostSchema = mongoose.model('Post');
var UserSchema = mongoose.model('User');
var CommentSchema = mongoose.model('Comment');



var SOFOOD_SECRET = '#rub_a_dubDub_thanks_forthe_grub!';

module.exports = function (app) {
    app.use(cors());
    app.use('/api/post', router);
};

router.post('/', expressJwt({ secret: SOFOOD_SECRET }), function(req, res, next) {

    var missing = [];
    if (!req.body.caption)
        missing.push("missing caption");
    if (!req.body.image)
        missing.push("missing image");
    if (missing.length) {
        return res.status(400).send({
            "status": 400,
            "err": 'Error: ' + missing.join(', ')
        });
    }

    UserSchema.findById(req.user._id, function(err, user) {
        if (err) return next(err);
        if (!user) return res.status(404).send({"err": "User not found."});

        var newPost = new PostSchema();
        newPost.user = req.user._id;
        newPost.caption = req.body.caption;
        newPost.image = req.body.image;
        newPost.macros = req.body.macros;

        newPost.save(function(err, post) {
            if (err) return next(err);

            FeedCtrl.trending.updateTree(post);
            return res.status(200).send(post);
        });
    });
});

router.get('/:postId', function(req, res, next) {
    PostSchema.findById(req.params.postId, function(err, post) {
        if (err) return next(err);
        if (!post) return res.status(404).send({"err": "Post not found."});

        return res.status(200).send(post);
    });
});

router.put('/:postId', expressJwt({ secret: SOFOOD_SECRET }), function(req, res, next) {

    var missing = [];
    if (!req.body.caption)
        missing.push("missing caption");
    if (!req.body.image)
        missing.push("missing image");
    if (req.body._id && req.body._id != req.params.postId)
        missing.push("found _id in body but does not match postId in url");
    if (missing.length) {
        return res.status(400).send({
            "status": 400,
            "err": 'Error: ' + missing.join(', ')
        });
    }
    PostSchema.findById(req.params.postId).populate('user').exec(function(err, post) {
        if (err) return next(err);
        if (!post) return res.status(404).send({"err": "Post not found."});

        post.user = req.user._id;
        post.caption = req.body.caption;
        post.image = req.body.image;
        post.macros = req.body.macros;

        post.save(function(err, updatedPost) {
            if (err) return next(err);

            FeedCtrl.trending.updateTree(updatedPost);
            updatedPost.userName = user.name;
            updatedPost.userPicture = user.picture;
            return res.status(200).send(updatedPost);
        });
    });
});

router.delete('/:postId', expressJwt({ secret: SOFOOD_SECRET }), function(req, res, next) {
    PostSchema.findById(req.params.postId, function(err, post) {
        if (err) return next(err);
        if (!post) return res.status(404).send({"err": "Post not found."});

        post.remove(function(err) {
            if (err) return next(err);

            FeedCtrl.trending.buildTree();
            return res.status(200).end();
        });
    });
});

// Query: ?page=1&per_page=10
router.get('/:postId/comments', function(req, res, next) {

    if (!req.query.page)
        return res.status(400).send({"err": "'page' query must be greater than 0"});
    else if (!req.query.per_page)
        return res.status(400).send({"err": "'per_page' query must be a number greater than 0"});

    PostSchema.findById({'_id': req.params.postId}, function(err, post) {
        if (err) next(err);
        if (!post) return res.status(404).send({"err":"post not found"});

        CommentSchema.find({'_id': {$in: post.comments}})
            .sort('-timeStamp.created')
            .skip((req.query.page - 1) * req.query.per_page)
            .limit(req.query.per_page)
            .exec(function(err, comments) {
                if (err) next(err);
                return res.status(200).send(comments);
            });
    });
});

router.put('/favorite/:postId', expressJwt({ secret: SOFOOD_SECRET }), function(req, res, next) {
    UserSchema.findById(req.user._id, function(err, user) {
        if (err) return next(err);
        if (!user) return res.status(404).send({"err": "User not found."});

        PostSchema.findById(req.params.postId, function(err, post) {
            if (err) return next(err);
            if (!post) return res.status(404).send({"err": "Post not found."});

            if (post.favoritedBy.users.indexOf(req.user._id) === -1) {
                post.favoritedBy.users.push(req.user._id);
                post.favoritedBy.count++;

                post.save(function(err, updatedPost) {
                    if (err) return next(err);

                    FeedCtrl.trending.updateTree(updatedPost);
                    return res.status(200).end();
                });
            }
            else {
                return res.status(400).send({"err": "Post already favorited by user."});
            }
        });
    });
});

router.put('/unfavorite/:postId', expressJwt({ secret: SOFOOD_SECRET }), function(req, res, next) {
    UserSchema.findById(req.user._id, function(err, user) {
        if (err) return next(err);
        if (!user) return res.status(404).send({"err": "User not found."});

        PostSchema.findById(req.params.postId, function(err, post) {
            if(err) return next(err);
            if (!post) return res.status(404).send({"err":"Post not found."});

            var userIndex = post.favoritedBy.users.indexOf(req.user._id);
            if (userIndex > -1) {
                post.favoritedBy.users.splice(userIndex, 1);
                post.favoritedBy.count--;

                post.save(function(err, updatedPost) {
                    if (err) return next(err);
                    FeedCtrl.trending.updateTree(updatedPost);
                    return res.status(200).end();
                });
            }
            else {
                return res.status(400).send({"err": "Post was not favorited by user."});
            }
        });
    });
});
