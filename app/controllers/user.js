var express = require('express'),
    router = express.Router(),
    cors = require('cors'),
    mongoose = require('mongoose'),
    jwt = require('jsonwebtoken'),
    expressJwt = require('express-jwt');

var UserSchema = mongoose.model('User');
var PostSchema = mongoose.model('Post');

var SOFOOD_SECRET = '#rub_a_dubDub_thanks_forthe_grub!';

/*
var FACEBOOK_APP_ID = "1073533659378132",
    FACEBOOK_APP_SECRET = "8e33a5835e63050d4aff46419df752b9";
    */

module.exports = function (app) {
    app.use(cors());
    app.use('/api/user', router);
};

function createJWT(user) {
    return jwt.sign({ _id: user._id }, SOFOOD_SECRET);
}

/*
*   POST - /api/user/auth
*
*       Handles both login and registration based on whether the user already
*       has an account with their FB email with us.
*/
router.post('/auth', function(req, res, next) {

    // Check if user already has an account by email.
    UserSchema.findOne({
        "facebook": req.body.facebook
    }, function(err, foundUser) {
        if (err) {
            return res.status(500).send(err);
        }

        // LOGIN
        else if (foundUser) {
            var token = createJWT(foundUser);
            return res.status(200).send({
                'token': token,
                'user': foundUser
            });
        }

        // REQUIRED FIELDS (to create a user)
        var missing = [];
        if (!req.body.name)
            missing.push("missing name");
        if (!req.body.email)
            missing.push("missing email");
        if (!req.body.facebook)
            missing.push("missing facebook");
        if (!req.body.picture)
            missing.push("missing picture");
        if (typeof req.body.picture !== 'string')
            missing.push("picture must be url string");
        if (missing.length) {
            return res.status(400).send({
                "status": 400,
                "err": 'Error: ' + missing.join(', ')
            });
        }

        // CREATE USER
        var newUser = new UserSchema();
        newUser.name = req.body.name;
        newUser.email = req.body.email;
        newUser.facebook = req.body.facebook;
        newUser.picture = req.body.picture;


        newUser.save(function(err, user) {
            if (err) {
                return res.status(500).send(err);
            }

            // Generate JWT
            var token = createJWT(newUser);

            res.status(200).send({
                'token': token,
                'user': user
            });
        });
    });
});

/*
*   GET - /api/user
*
*       Returns the current user. Must be logged in.
*/
router.get('/', expressJwt({ secret: SOFOOD_SECRET }), function(req, res, next) {

    UserSchema.findOne({ '_id': req.user._id }, function (err, user) {
        if (err) return next(err);
        if (user)
            return res.status(200).send(user);
        res.status(400).end();
    });
});

router.get('/search', function(req, res, next) {

    UserSchema
        .find({'name' : new RegExp(req.query.term, 'i')})
        .limit(50)
        .exec(function(err, results){
            if (err) return next(err);
            return res.status(200).send(results);
        });

    // UserSchema.find({$text: {$search: req.query.term}}, function(err, results) {
    //     if (err) return next(err);
    //     return res.status(200).send(results);
    // });
});

router.get('/followers/:userId', function(req, res, next) {

    UserSchema.findOne({ '_id': req.params.userId }, function (err, user) {
        if (err) return next(err);
        if (user)
            return res.status(200).send(user.followers);
        res.status(400).end();
    });
});

router.get('/following/:userId', function(req, res, next) {

    UserSchema.findOne({ '_id': req.params.userId }, function (err, user) {
        if (err) return next(err);
        if (user)
            return res.status(200).send(user.following);
        res.status(400).end();
    });
});

/*
*   PUT - /api/user
*
*       Update the current user. Must be logged in.
*/
router.put('/', expressJwt({ secret: SOFOOD_SECRET }), function(req, res, next) {

    console.log(req.body.user);

    // REQUIRED FIELDS (to create a user)
    var missing = [];
    if (!req.body.picture)
        missing.push("missing picture");
    if (!req.body.facebook)
        missing.push("missing facebook");
    if (!req.body.email)
        missing.push("missing email");
    if (!req.body.name)
        missing.push("missing name");
    if (!req.body.comments)
        missing.push("missing comments");
    if (!req.body.posts)
        missing.push("missing posts");
    if (missing.length) {
        return res.status(400).send({
            "status": 400,
            "err": 'Error: ' + missing.join(', ')
        });
    }
    else {
        UserSchema.findById(req.user._id, function(err, user) {
            if (!user || err) {
                return res.status(404).send({
                    "status": 404,
                    "err": 'Error: ' + 'userId not found.'
                });
            }

            user.picture = req.body.picture;
            user.facebook = req.body.facebook;
            user.email = req.body.email;
            user.name = req.body.name;
            user.comments = req.body.comments;
            user.posts = req.body.posts;

            user.save(function(err, updatedUser) {
                if (err)
                    return res.status(400).end();
                return res.status(200).send(updatedUser);
            });
        });
    }
});

/*
*   DELETE - /api/user
*
*       Delete the current user. Must be logged in.
*/
router.delete('/', expressJwt({ secret: SOFOOD_SECRET }), function(req, res, next) {
    UserSchema.findById(req.user._id, function(err, user) {
        if (!user || err)
            return res.status(404).send({
                "status": 404,
                "err": 'Error: ' + 'userId not found.'
            });

        user.remove(function(err) {
            if (err) {
                return res.status(500).end();
            }
            return res.status(200).end();
        });
    });
});

/*
*   GET - /api/user/{userId}
*
*       Find and return a user by their userId. Must be logged in.
*/
router.get('/:userId', expressJwt({ secret: SOFOOD_SECRET }), function(req, res, next) {
    UserSchema.findById(req.params.userId, function (err, user) {
        if (err) return next(err);
        if (user)
            return res.status(200).send(user);
        res.status(400).end();
    });
});

/*
*   GET - /api/user/{userId}/posts
*
*       Find and return a user's posts. Must be logged in.
*/
router.get('/:userId/posts', expressJwt({ secret: SOFOOD_SECRET }), function(req, res, next) {

    if (!req.query.page)
        return res.status(400).send({"err": "'page' query must be greater than 0"});
    else if (!req.query.per_page)
        return res.status(400).send({"err": "'per_page' query must be a number greater than 0"});

    UserSchema.findById(req.params.userId).exec(function(user) {
        PostSchema.find({ 'user': req.params.userId })
            .sort('-timeStamp.created')
            .skip((req.query.page - 1) * req.query.per_page)
            .limit(req.query.per_page)
            .populate('user')
            .exec(function(err, posts) {
                if (err) return next(err);
                for (var i = 0; i < posts.length; i = i + 1) {
                    posts[i] = posts[i].toJSON();
                    posts[i].userName = posts[i].user.name[0];
                    posts[i].userPicture = posts[i].user.picture;
                    posts[i].user = posts[i].user._id;
                }
                return res.status(200).send(posts);
            });
    });
});

/*
*   GET - /api/user/isfollowing/{userId}
*
*       Check if we are following a user. Must be logged in.
*/
router.get('/isfollowing/:userId', expressJwt({ secret: SOFOOD_SECRET }), function(req, res, next) {
    if (req.params.userId === req.user._id)
        return res.status(400).send({
            "status": 400,
            "err": 'Error: You cannot follow yourself.'
        });

    UserSchema.findById(req.user._id, function(err, currentUser) {
        var index = -1;
        if (currentUser && currentUser.following) {
            index = currentUser.following.indexOf(req.params.userId);
        }
        else {
            return res.status(400).end();
        }

        if (index > -1) {
            return res.status(200).send(true);
        }
        else {
            return res.status(200).send(false);
        }
    });
});

/*
*   PUT - /api/user/follow/{userId}
*
*       Follow a user. Must be logged in.
*/
router.put('/follow/:userId', expressJwt({ secret: SOFOOD_SECRET }), function(req, res, next) {
    if (req.params.userId === req.user._id)
        return res.status(400).send({
            "status": 400,
            "err": 'Error: You cannot follow yourself.'
        });

    UserSchema.findById(req.user._id, function(err, currentUser) {
        if (err) return res.status(500).send(err);
        if (!currentUser)
            return res.status(400).send({
                "status": 400,
                "err": 'Error: Current user not found.'
            });

        UserSchema.findById(req.params.userId, function(err, otherUser) {
            if (err) return res.status(500).send(err);
            if (!otherUser)
                return res.status(400).send({
                    "status": 400,
                    "err": 'Error: User to follow not found.'
                });

            currentUser.following.push(otherUser._id);
            otherUser.followers.push(currentUser._id);

            // Remove any duplicates (if following more than once)
            currentUser.following = currentUser.following.filter(onlyUnique);
            otherUser.followers = otherUser.followers.filter(onlyUnique);

            currentUser.save(function(err, user) {
                if (err) return res.status(500).send(err);
                otherUser.save(function(err) {
                    if (err) return res.status(500).send(err);
                    return res.status(200).send(user);
                });
            });
        });
    });
});

/*
*   PUT - /api/user/unfollow/{userId}
*
*       Unfollow a user. Must be logged in.
*/
router.put('/unfollow/:userId', expressJwt({ secret: SOFOOD_SECRET }), function(req, res, next) {
    if (req.params.userId === req.user._id)
        return res.status(400).send({
            "status": 400,
            "err": 'Error: You cannot unfollow yourself.'
        });

    UserSchema.findById(req.user._id, function(err, currentUser) {
        if (err) return res.status(500).send(err);
        if (!currentUser)
            return res.status(400).send({
                "status": 400,
                "err": 'Error: Current user not found.'
            });

        UserSchema.findById(req.params.userId, function(err, otherUser) {
            if (err) return res.status(500).send(err);
            if (!otherUser)
                return res.status(400).send({
                    "status": 400,
                    "err": 'Error: User to follow not found.'
                });

            var index;
            index = currentUser.following.indexOf(otherUser._id);
            if (index > -1)
                currentUser.following.splice(index, 1);
            else {
                return res.status(400).send({
                    "status": 400,
                    "err": 'Error: User was not following the other user, cannot unfollow.'
                });
            }

            index = otherUser.followers.indexOf(currentUser._id);
            if (index > -1)
                otherUser.followers.splice(index, 1);

            // Remove any duplicates (if following more than once)
            currentUser.following = currentUser.following.filter(onlyUnique);
            otherUser.followers = otherUser.followers.filter(onlyUnique);

            currentUser.save(function(err, user) {
                if (err) return res.status(500).send(err);
                otherUser.save(function(err) {
                    if (err) return res.status(500).send(err);
                    return res.status(200).send(user);
                });
            });
        });
    });
});

// Utility Functions
function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}
