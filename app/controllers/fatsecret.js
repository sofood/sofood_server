var express = require('express'),
    router = express.Router(),
    cors = require('cors'),
    mongoose = require('mongoose'),
    jwt = require('jsonwebtoken'),
    expressJwt = require('express-jwt'),
    request = require('request'),
    crypto = require('crypto'),
    rest = require('restler'),
    apiKey = 'ecbfaa058e0742c888f27bf2b24d0a70',
    sharedSecret = 'a1fc4e561b804bb383785a9938b88fe9',
    fatSecretRestUrl = 'http://platform.fatsecret.com/rest/server.api';

var PostSchema = mongoose.model('Post');
var UserSchema = mongoose.model('User');

var SOFOOD_SECRET = '#rub_a_dubDub_thanks_forthe_grub!';

module.exports = function (app) {
    app.use(cors());
    app.use('/api/fatsecret', router);
};

router.get('/search/:term', expressJwt({ secret: SOFOOD_SECRET }), function(req, res, next) {

    var term = req.params.term.replace(/ /g,".");

    var date = new Date();
    var reqObj = {
        format: 'json',
        method: 'foods.search',
        oauth_consumer_key: apiKey,
        oauth_nonce: Math.random().toString(36).replace(/[^a-z]/,'').substr(2),
        oauth_signature_method: 'HMAC-SHA1',
        oauth_timestamp: Math.floor(date.getTime() / 1000),
        oauth_version: '1.0',
        search_expression: term
    };

    var paramsStr = '';
    for (var i in reqObj) {
        paramsStr += "&" + i + "=" + reqObj[i];
    }
    paramsStr = paramsStr.substr(1);

    var sigBaseStr = "POST&" + encodeURIComponent(fatSecretRestUrl) + "&" + encodeURIComponent(paramsStr);
    var modSharedSecret = sharedSecret + "&";
    var hashedBaseStr = crypto.createHmac('sha1', modSharedSecret).update(sigBaseStr).digest('base64');

    reqObj.oauth_signature = hashedBaseStr;

    rest.post(fatSecretRestUrl, {
        data: reqObj
    }).on('complete', function(data, response) {
        if (data&&data.foods&&data.foods.food) {
            res.status(200).send(data.foods.food);
        }
        else {
            res.status(400).send([]);
        }

    });
});

router.get('/food', expressJwt({ secret: SOFOOD_SECRET }), function(req, res, next) {

    var missing = [];

    console.log(req.query.unit);

    if (!req.query.food_id)
        missing.push("missing food_id");
    if (!req.query.unit)
        missing.push("missing unit");
    if (!req.query.amount)
        missing.push("missing amount");
    if (req.query.amount<0)
        missing.push("amount less than 0");
    // if ((req.query.unit!=='kg')||(req.query.unit!=='lb')||(req.query.unit!=='g'))
    //     missing.push("unsupported unit: " + req.query.unit);
    if (missing.length) {
        return res.status(400).send({
            "status": 400,
            "err": 'Error: ' + missing.join(', ')
        });
    }

    var date = new Date();
    var reqObj = {
        food_id: req.query.food_id,
        format: 'json',
        method: 'food.get',
        oauth_consumer_key: apiKey,
        oauth_nonce: Math.random().toString(36).replace(/[^a-z]/,'').substr(2),
        oauth_signature_method: 'HMAC-SHA1',
        oauth_timestamp: Math.floor(date.getTime() / 1000),
        oauth_version: '1.0',
    };

    var paramsStr = '';
    for (var i in reqObj) {
        paramsStr += "&" + i + "=" + reqObj[i];
    }
    paramsStr = paramsStr.substr(1);

    var sigBaseStr = "POST&" + encodeURIComponent(fatSecretRestUrl) + "&" + encodeURIComponent(paramsStr);
    var modSharedSecret = sharedSecret + "&";
    var hashedBaseStr = crypto.createHmac('sha1', modSharedSecret).update(sigBaseStr).digest('base64');

    reqObj.oauth_signature = hashedBaseStr;

    rest.post(fatSecretRestUrl, {
        data: reqObj
    }).on('complete', function(data, response) {
        if (data.error) return res.status(500).send(data);

        res.status(200).send(getMacros(data.food.servings.serving, req.query.unit, req.query.amount));
    });
});

var getMacros = function(serving, unit, amount) {

    var macros = {
        calories: 0,
        protein: 0,
        carbohydrate: 0,
        fat: 0
    };
    var inGrams = amount;

    var getRightServing = function (serving) {
        for (var index in serving) {
            if (serving[index].metric_serving_unit === 'g') {
                return serving[index];
            }
        }
        return serving[0];
    };

    if (unit === 'lb') {
        inGrams = amount * 453.592;
    }
    if (unit === 'kg') {
        inGrams = amount * 1000;
    }

    var rightServing = getRightServing(serving);
    var macroMultiplier = 1;

    if (rightServing) {
        macroMultiplier = inGrams/rightServing.metric_serving_amount;
        macros.calories = rightServing.calories * macroMultiplier;
        macros.protein = rightServing.protein * macroMultiplier;
        macros.carbohydrate = rightServing.carbohydrate * macroMultiplier;
        macros.fat = rightServing.fat * macroMultiplier;
    }

    return macros;
};
