var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var relationship = require('mongoose-relationship');

// MONGOOSE-RELATIONSHIP
// Parent to: Comments
// Child to: Users

var PostSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        childPath: "posts"
    },
    caption: String,
    popularity: Number,
    image: String,
    macros: {
        calories: Number,
        protein: Number,
        carbohydrates: Number,
        fats: Number
    },
    favoritedBy: {
        count: Number,
        users: [{
            type: Schema.Types.ObjectId,
            ref: "User",
            childPath: "favorites"
        }]
    },
    comments: [{
        type: Schema.Types.ObjectId,
        ref: "Comment"
    }],
    timeStamp: {
        created: {type: Date, default: Date.now},
        updated: {type: Date, default: Date.now}
    }
});

PostSchema.plugin(relationship, { relationshipPathName: ['user', 'favoritedBy.users'] });

// Updates timeStamp.updated every time the document is changed.
PostSchema.pre('save', function (next) {
    this.timeStamp.updated = new Date();
    this.favoritedBy.count = this.favoritedBy.users.length;
    calculatePopularity(this);
    next();
});

/*
PostsSchema.virtual('date')
  .get(function(){
    return this._id.getTimestamp();
  });
  */

mongoose.model('Post', PostSchema);

var calculatePopularity = function(post) {
    var logBaseN = function(num, base) {
        return Math.log(num) / Math.log(base);
    };
    var timeDivisor = 75000;
    var favoriteMultiplier = 1.1;
    var logBase = 10;

    var unixTimeStamp = Date.parse(post.timeStamp.created);

    var favPortion = 0;

    if (post.favoritedBy.count) {
        favPortion = favoriteMultiplier * logBaseN(post.favoritedBy.count, logBase);
    }

    post.popularity = favPortion + (unixTimeStamp / timeDivisor);
};
