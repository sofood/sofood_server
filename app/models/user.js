var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var relationship = require('mongoose-relationship');

// MONGOOSE-RELATIONSHIP
// Parent to: Posts, Comments
// Child to: NONE

var UserSchema = new Schema({
    name: {type: [String], index: true},
    email: String,
    height: Number,
    weight: Number,
    dob: Date,
    sex: String,
    facebook: String, // facebook id
    picture: String,
    following: [{
        type: Schema.Types.ObjectId,
        ref: "User"
    }],
    followers: [{
        type: Schema.Types.ObjectId,
        ref: "User"
    }],
    posts: [{
        type: Schema.Types.ObjectId,
        ref: "Post"
    }],
    favorites: [{
        type: Schema.Types.ObjectId,
        ref: "Post"
    }],
    comments: [{
        type: Schema.Types.ObjectId,
        ref: "Comment"
    }],
    timeStamp: {
        created: {type: Date, default: Date.now},
        updated: {type: Date, default: Date.now}
    }
});

// Updates timeStamp.updated every time the document is changed.
UserSchema.pre('save', function (next) {
    this.timeStamp.updated = new Date();
    next();
});

/*
staffSchema.plugin(relationship, { relationshipPathName: 'groups' });
*/

/*
UsersSchema.virtual('date')
  .get(function(){
    return this._id.getTimestamp();
  });
  */

mongoose.model('User', UserSchema);
