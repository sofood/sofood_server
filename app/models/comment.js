var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var relationship = require('mongoose-relationship');

// MONGOOSE-RELATIONSHIP
// Parent to: NONE
// Child to: Users, Posts

var CommentSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        childPath: "comments"
    },
    post: {
        type: Schema.Types.ObjectId,
        ref: "Post",
        childPath: "comments"
    },
    text: String,
    timeStamp: {
        created: {type: Date, default: Date.now},
        updated: {type: Date, default: Date.now}
    }
});

CommentSchema.plugin(relationship, { relationshipPathName: ['post', 'user'] });

// Updates timeStamp.updated every time the document is changed.
CommentSchema.pre('save', function (next) {
    this.timeStamp.updated = new Date();
    next();
});

/*
CommentsSchema.virtual('date')
  .get(function(){
    return this._id.getTimestamp();
  });
  */

mongoose.model('Comment', CommentSchema);
