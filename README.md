# sofood_server #
-----------------

### What is this? ###
* **sofood_server** is the backend for **sofood_android**
* Its technology is based on: Node.js, Express, connecting to a remote MongoDB server.
* The server focuses on providing a public-facing Express API that allows **sofood_android** to interact with the database.

### How to Run First Time ###
* Make sure you have Node.js installed.
* Change directories into cloned repo.
* Install Gulp globally (if you haven't already):
```
npm install -g gulp
```
* Install npm depedencies:
```
npm install
```
* Gulp
```
gulp
```
* Express is now listening on **localhost:2999**

### How to Run After First Time ###
* Gulp
```
gulp
```

### MongoDB ###
* URI
    * mongodb://sofoodAdmin:!sofoodAdmin#@192.241.202.71:27017/admin
* Server
    * 192.241.202.71:27017
* User DB
    * admin
* User name
    * sofoodAdmin
* Password
    * !sofoodAdmin#